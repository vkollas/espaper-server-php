<?php

	// All credit goes to @squix78 for this code.  I am just slowing learning and
	// making changes to customize his original ESPaper Display
	// his original code can be downloaded from https://github.com/squix78?tab=repositories
	// this project is based on the espaper-server-php repository
	// You can purchase the ESpaper kit at https://blog.squix.org/product/2-9-espaper-plus-kit

    include('ESPaperCanvas.php');

    date_default_timezone_set("America/New_York");
    $WUNDERGROUND_API_KEY = "YOUR_API_KEY_HERE"; // change to your API key
    $WUNDERGROUND_URL = "http://api.wunderground.com/api/"; // DO NOT CHANGE
    $WUNDERGROUND_STATION_URL = "/q/CA/San_Francisco.json"; // Adjust to desired data per API help. Generic from API help
	// to pull data from Personal Weather Station use "/q/CA/pws:YOUR_STATION_ID.json"
	
    $output = $_GET["output"];
    if (!isset($output) || $output === "json") {
      $isJson = true;
    } else {
      $isJson = false;
    }
    if ($isJson) {
    	header('Content-Type: application/json');
    } else {
      header('Content-Type: image/svg+xml');
    }
  	$battery = $_GET['battery'] / 1024.0;
  	$voltage = round(($battery + 0.083) * 13 / 3.0, 2);
  	if ($voltage > 4.2) {
  		$percentage = 100;
  	} else if ($voltage < 3) {
  		$percentage = 0;
  	} else {
  		$percentage = round(($voltage - 3.2) * 100 / (4.2 - 3.0));
  	}

  	$meteocons = array(
  		"chanceflurries" => "F",
  		"chancerain" => "Q",
  		"chancesleet" => "W",
  		"chancesnow" => "V",
  		"chancetstorms" => "S",
  		"clear" => "B",
  		"cloudy" => "Y",
  		"flurries" => "F",
  		"fog" => "M",
  		"hazy" => "E",
  		"mostlycloudy" => "Y",
  		"mostlysunny" => "H",
  		"partlycloudy" => "H",
  		"partlysunny" => "J",
  		"sleet" => "W",
  		"rain" => "R",
  		"snow" => "W",
  		"sunny" => "B",
  		"tstorms" => "0",
  		"nt_chanceflurries" => "F",
  		"nt_chancerain" => "7",
  		"nt_chancesleet" => "#",
  		"nt_chancesnow" => "#",
  		"nt_chancetstorms" => "&",
  		"nt_clear" => "2",
  		"nt_cloudy" => "Y",
  		"nt_flurries" => "9",
  		"nt_fog" => "M",
  		"nt_hazy" => "E",
  		"nt_mostlycloudy" => "5",
  		"nt_mostlysunny" => "3",
  		"nt_partlycloudy" => "4",
  		"nt_partlysunny" => "4",
  		"nt_sleet" => "9",
  		"nt_rain" => "7",
  		"nt_snow" => "#",
  		"nt_sunny" => "4",
  		"nt_tstorms" => "&"
  	);
	
	  	
	// conditions test file - just downloaded to my webserver for testing rather
	// than using up my API calls to WUNDERGROUND

	// conditions test file
	// $json_string1 = file_get_contents("http://192.168.1.50/conditions.json");

	// download real JSON File
	$json_string1 = file_get_contents($WUNDERGROUND_URL.$WUNDERGROUND_API_KEY."/conditions".$WUNDERGROUND_STATION_URL);
	$result1 = json_decode($json_string1);
  	$condition = $result1->{'current_observation'};
  	
	// hourly test file - just downloaded to my webserver for testing rather
	// than using up my API calls to WUNDERGROUND

	// hourly test file
	// $json_string2 = file_get_contents("http://192.168.1.50/hourly.json");

	// download real JSON File
	$json_string2 = file_get_contents($WUNDERGROUND_URL.$WUNDERGROUND_API_KEY."/hourly".$WUNDERGROUND_STATION_URL);
	$result2 = json_decode($json_string2);
	$hourly = $result2->{'hourly_forecast'};
  	
	// astronomy test file - just downloaded to my webserver for testing rather
	// than using up my API calls to WUNDERGROUND

	// astronomy test file
	// $json_string3 = file_get_contents("http://192.168.1.50/astronomy.json");

	// download real JSON File
	$json_string3 = file_get_contents($WUNDERGROUND_URL.$WUNDERGROUND_API_KEY."/astronomy".$WUNDERGROUND_STATION_URL);
	$result3 = json_decode($json_string3);
	$astronomy = $result3->{'moon_phase'};

	// forecast test file - just downloaded to my webserver for testing rather
	// than using up my API calls to WUNDERGROUND
	// forecast test file
	// $json_string4 = file_get_contents("http://192.168.1.50/forecast.json");

	// download real JSON File
	$json_string4 = file_get_contents($WUNDERGROUND_URL.$WUNDERGROUND_API_KEY."/forecast".$WUNDERGROUND_STATION_URL);
	$result4 = json_decode($json_string4, true);
	$daily = $result4['forecast']['simpleforecast']['forecastday'];

// Not using moon data or graphs right now
/*

  	$moon_age_char = chr(65 + 26 * (($astronomy->{'ageOfMoon'} % 30) / 30.0));

  	$min_temp = 999;
  	$max_temp = -999;
    $sum_temp = 0;
  	$temps = array();
  	for ($i = 0; $i < sizeof($hourly); $i++) {
  		$temp = $hourly[$i]->{'temp'}->{'english'};
  		if ($temp < $min_temp) {
  			$min_temp = $temp;
  		}
  		if ($temp > $max_temp) {
  			$max_temp = $temp;
  		}
      $sum_temp += temp;
  		array_push($temps, $temp);
  	}
    $avg_temp = $sum_temp / sizeof($hourly);
*/
    $canvas = new Canvas(296, 128);
    $canvas->setJson($isJson);
    $canvas->start();
    $canvas->registerFont($baseUrl."fonts/", "Meteocons", "Plain", 42, "MeteoconsPlain42");
    $canvas->registerFont($baseUrl."fonts/", "Meteocons", "Plain", 21, "MeteoconsPlain21");
    $canvas->registerFont($baseUrl."fonts/", "Moonphases", "Plain", 36, "MoonphasesPlain36");

    $canvas->registerFont($baseUrl."fonts/", "Arial", "Plain", 10, "ArialPlain10");
    $canvas->registerFont($baseUrl."fonts/", "Arial", "Plain", 16, "ArialPlain16");
    $canvas->registerFont($baseUrl."fonts/", "Arial", "Plain", 24, "ArialPlain24");


		$canvas->fillBuffer(1);
		$canvas->setFont("ArialPlain10");
		$canvas->setTextAlignment("LEFT");
		$canvas->drawString(2, -2, "Updated: ".date("Y-m-d H:i:s"));
		$canvas->drawLine(0, 11, 296, 11);
		$canvas->setTextAlignment("RIGHT");
		$canvas->drawString(274, -1, $voltage. "V ".$percentage."%");
		$canvas->drawRect(274, 0, 18, 10);
		$canvas->drawRect(293, 2, 1, 6);
		$canvas->fillRect(276, 2, round(14 * $percentage / 100), 6);
		$canvas->setTextAlignment("LEFT");
		$canvas->setFont("MeteoconsPlain42");
		$canvas->drawString(5, 11, $meteocons[$condition->{'icon'}]);
		$canvas->setFont("ArialPlain10");
		$canvas->drawString(55, 15, $condition->{'display_location'}->{'city'});
		$canvas->drawString(55, 50, $condition->{'weather'});
		$canvas->setFont("ArialPlain24");
		$canvas->drawString(55, 25, $condition->{'temp_f'}."°F");
		$canvas->drawLine(0, 65, 296, 65);

		// starting my own lines & data
		// big blocks for wind data
		$canvas->drawLine(130, 12, 130, 65);
		$canvas->setFont("ArialPlain10");
        $canvas->setTextAlignment("CENTER");
		$canvas->drawString(158, 15, "Wind");
		$canvas->setFont("ArialPlain24");
		$canvas->drawString(158, 25, $condition->{'wind_mph'});
		$canvas->setFont("ArialPlain10");
		$canvas->drawString(158, 50, $condition->{'wind_dir'});
		$canvas->drawLine(187, 20, 187, 55);
		$canvas->drawString(216, 15, "Gust");
		$canvas->setFont("ArialPlain24");
		$canvas->drawString(216, 25, $condition->{'wind_gust_mph'});
		$canvas->setFont("ArialPlain10");
		$canvas->drawString(216, 50, "MPH");
		$canvas->drawLine(245, 12, 245, 65);
		$canvas->drawString(271, 15, "Windchill");
		$canvas->drawString(271, 50, "°F");
		$canvas->setFont("ArialPlain24");
		$canvas->drawString(271, 25, $condition->{'windchill_f'});


		// code for 3 day forecast
		//test
    $canvas->setTextAlignment("BOTH");
		$canvas->setFont("ArialPlain16");
		$canvas->drawLine(75, 80, 296, 80);
		$canvas->drawLine(75, 65, 75, 116);
		$canvas->drawLine(149, 65, 149, 116);
		$canvas->drawLine(223, 65, 223, 116);
		$canvas->drawString(37, 70, "3 - Day");
		$canvas->drawString(37, 90, "Forecast");
    $canvas->setTextAlignment("LEFT");
		$canvas->setFont("ArialPlain10");
		$canvas->drawString(79, 67, $daily[0]['date']['weekday']);
		$canvas->drawString(153, 67, $daily[1]['date']['weekday']);
		$canvas->drawString(227, 67, $daily[2]['date']['weekday']);
    $canvas->setTextAlignment("BOTH");
    $canvas->setFont("MeteoconsPlain21");
		$canvas->drawString(82, 86, $meteocons[$daily[0]['icon']]);
		$canvas->drawString(155, 86, $meteocons[$daily[1]['icon']]);
		$canvas->drawString(229, 86, $meteocons[$daily[2]['icon']]);
    $canvas->setTextAlignment("LEFT");
    $canvas->setFont("ArialPlain10");
		$canvas->drawString(108, 83, "LO: ". $daily[0]['low']['fahrenheit']."°");
		$canvas->drawString(108, 100, " HI:  ". $daily[0]['high']['fahrenheit']."°");
		$canvas->drawString(180, 83, "LO: ". $daily[1]['low']['fahrenheit']."°");
		$canvas->drawString(180, 100, " HI:  ". $daily[1]['high']['fahrenheit']."°");
		$canvas->drawString(255, 83, "LO: ". $daily[2]['low']['fahrenheit']."°");
		$canvas->drawString(255, 100, " HI:  ". $daily[2]['high']['fahrenheit']."°");



		// original code for hourly forcast
/*
		function DrawForecastDetail($x, $y, $hourForecast) {
				global $meteocons, $canvas;
				$canvas->setFont("ArialPlain10");
				$canvas->setTextAlignment("CENTER");
				$canvas->drawString($x + 25, $y -  2, $hourForecast->{'FCTTIME'}->{'hour_padded'}.":00");
				$canvas->drawString($x + 25, $y + 12, $hourForecast->{'temp'}->{'english'} . "° " . $hourForecast->{'pop'} . "%");
				$canvas->setFont("MeteoconsPlain21");
				$canvas->drawString($x + 25, $y + 24, $meteocons[$hourForecast->{'icon'}]);
				$canvas->drawLine($x + 2, 12, $x + 2, 65);
				$canvas->drawLine($x + 2, 25, $x + 43, 25);

		}
		 drawForecastDetail(296 / 2 -  20, 15, $hourly[0]);
		 drawForecastDetail(296 / 2 +  22, 15, $hourly[3]);
		 drawForecastDetail(296 / 2 +  64, 15, $hourly[6]);
		 drawForecastDetail(296 / 2 + 106, 15, $hourly[9]);

    
    // original code for min/max temp grpah

    // original code for min/max temp graph
    function tempToPixel($height, $max, $min, $temp) {
      return $temp * $height / ($max - $min);
    }

	 for ($i = 0; $i < min(32, sizeof($temps)); $i++) {
      $height = tempToPixel(13, $max_temp, $min_temp, $temps[$i]);
			$canvas->fillRect(200 + $i * 3, 116 - $height, 2, $height);
	 	}
	// original code for moon data
    $canvas->setTextAlignment("LEFT");
    $canvas->setFont("ArialPlain10");
    $canvas->drawString(200, 65, "Min:".$min_temp."°F   Max:".$max_temp."°F");
	$canvas->drawLine(196, 65, 196, 284);
	$canvas->setFont("MoonphasesPlain36");
	$canvas->setTextAlignment("LEFT");
	$canvas->drawString(5, 72, $moon_age_char);
	$canvas->setFont("ArialPlain10");
	$canvas->drawString(55, 72, "Sun:");
	$canvas->drawString(95, 72,
			$astronomy->{'sunrise'}->{'hour'} . ":". $astronomy->{'sunrise'}->{'minute'}." - "
		  .$astronomy->{'sunset'}->{'hour'} . ":". $astronomy->{'sunset'}->{'minute'});
	$canvas->drawString(55, 84, "Moon:");
	$canvas->drawString(95, 84,
			$astronomy->{'moonrise'}->{'hour'} . ":" . $astronomy->{'moonrise'}->{'minute'}." - "
			.$astronomy->{'moonset'}->{'hour'} . ":" . $astronomy->{'moonset'}->{'minute'});
	$canvas->drawString(55, 96, "Phase:");
	$canvas->drawString(95, 96, $astronomy->{'phaseofMoon'});
*/

	// code for reset line
	$canvas->setFont("ArialPlain10");
	$canvas->setTextAlignment("CENTER");
		$canvas->drawString(48, 116, "CONFIG+RST");
		$canvas->drawString(245, 116, "UPDATE");
		$canvas->drawLine(0, 116, 296, 116);
		$canvas->drawLine(98, 116, 98, 128);
		$canvas->drawLine(196, 116, 196, 128);
		$canvas->commit();
    $canvas->end();

		?>
